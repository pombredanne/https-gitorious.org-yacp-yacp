clutter-gesture
------------------------------------------
Clutter gesture implements a gesture-recognition framework on top of Clutter,
which has multi-point input events as input, then, after matching a
pre-defined gesture, emits the gesture events to the applications.

Runtime requirements:
  cygwin-1.7.7-1
  libclutter1.0_0-1.2.12-1
  libgcc1-4.3.4-3
  libglib2.0_0-2.24.1-1

Build requirements:
(besides corresponding -devel packages)
  autoconf-9-1
  automake-4-10
  binutils-2.20.51-2
  cygport-0.10.2-1
  gawk-3.1.8-1
  gcc4-core-4.3.4-3
  libtool-2.4-1
  make-3.81-2

Canonical website:
  http://moblin.org/projects/clutter-gesture

Canonical download:
  http://git.moblin.org/cgit.cgi/clutter-gesture/snapshot/clutter-gesture-0.0.2.1.tar.bz2

-------------------------------------------

Build instructions:
  unpack clutter-gesture-0.0.2.1-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./clutter-gesture-0.0.2.1-X.cygport all

This will create:
  /usr/src/clutter-gesture-0.0.2.1-X-src.tar.bz2
  /usr/src/clutter-gesture-0.0.2.1-X.tar.bz2
  /usr/src/libcluttergesture0.0.2_0-0.0.2.1-X.tar.bz2
  /usr/src/libcluttergesture0.0.2-devel-0.0.2.1-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(clutter-gesture)
  /usr/share/doc/Cygwin/clutter-gesture.README
  /usr/share/doc/clutter-gesture/README

(libcluttergesture0.0.2_0)
  /usr/bin/cygcluttergesture-0.0.2-0.dll
  /usr/bin/cygengine-0.dll

(libcluttergesture0.0.2-devel)
  /usr/include/clutter-gesture/clutter-gesture.h
  /usr/include/clutter-gesture/gesture-events.h
  /usr/lib/libcluttergesture-0.0.2.a
  /usr/lib/libcluttergesture-0.0.2.dll.a
  /usr/lib/libcluttergesture-0.0.2.la
  /usr/lib/libengine.a
  /usr/lib/libengine.dll.a
  /usr/lib/libengine.la
  /usr/lib/pkgconfig/clutter-gesture.pc

------------------

Port Notes:

----- version 0.0.2.1-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

