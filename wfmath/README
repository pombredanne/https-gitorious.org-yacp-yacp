wfmath
------------------------------------------
WFMath provides mathematical functions for WorldForge clients. The primary
focus of WFMath is geometric objects. Thus, it includes several shapes (boxes,
balls, lines), in addition to the basic math objects that are used to build
these shapes (points, vectors, matricies). WFMath provides a means for other
system compenents to pass geometric information around in a common format.

Runtime requirements:
  cygwin-1.7.10-1
  libgcc1-4.5.3-3
  libstdc++6-4.5.3-3

Build requirements:
(besides corresponding -devel packages)
  autoconf-10-1
  automake-4-10
  binutils-2.22.51-1
  cygport-0.10.7-1
  gawk-4.0.0-1
  gcc4-core-4.5.3-3
  gcc4-g++-4.5.3-3
  libtool-2.4-1
  make-3.82.90-1

Canonical website:
  http://worldforge.org/dev/eng/libraries/wfmath

Canonical download:
  mirror://sourceforge/worldforge/wfmath-0.3.12.tar.bz2

-------------------------------------------

Build instructions:
  unpack wfmath-0.3.12-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./wfmath-0.3.12-X.cygport all

This will create:
  /usr/src/wfmath-0.3.12-X-src.tar.bz2
  /usr/src/wfmath-0.3.12-X.tar.bz2
  /usr/src/libwfmath0.3_6-0.3.12-X.tar.bz2
  /usr/src/libwfmath0.3-devel-0.3.12-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(wfmath)
  /usr/share/doc/Cygwin/wfmath.README
  /usr/share/doc/wfmath/AUTHORS
  /usr/share/doc/wfmath/COPYING
  /usr/share/doc/wfmath/ChangeLog
  /usr/share/doc/wfmath/NEWS
  /usr/share/doc/wfmath/README
  /usr/share/doc/wfmath/TODO

(libwfmath0.3_6)
  /usr/bin/cygwfmath-0.3-6.dll

(libwfmath0.3-devel)
  /usr/include/wfmath-0.3/wfmath/MersenneTwister.h
  /usr/include/wfmath-0.3/wfmath/atlasconv.h
  /usr/include/wfmath-0.3/wfmath/axisbox.h
  /usr/include/wfmath-0.3/wfmath/axisbox_funcs.h
  /usr/include/wfmath-0.3/wfmath/ball.h
  /usr/include/wfmath-0.3/wfmath/ball_funcs.h
  /usr/include/wfmath-0.3/wfmath/const.h
  /usr/include/wfmath-0.3/wfmath/error.h
  /usr/include/wfmath-0.3/wfmath/int_to_string.h
  /usr/include/wfmath-0.3/wfmath/intersect.h
  /usr/include/wfmath-0.3/wfmath/intersect_decls.h
  /usr/include/wfmath-0.3/wfmath/miniball.h
  /usr/include/wfmath-0.3/wfmath/miniball_funcs.h
  /usr/include/wfmath-0.3/wfmath/point.h
  /usr/include/wfmath-0.3/wfmath/point_funcs.h
  /usr/include/wfmath-0.3/wfmath/polygon.h
  /usr/include/wfmath-0.3/wfmath/polygon_funcs.h
  /usr/include/wfmath-0.3/wfmath/polygon_intersect.h
  /usr/include/wfmath-0.3/wfmath/probability.h
  /usr/include/wfmath-0.3/wfmath/quaternion.h
  /usr/include/wfmath-0.3/wfmath/randgen.h
  /usr/include/wfmath-0.3/wfmath/rotbox.h
  /usr/include/wfmath-0.3/wfmath/rotbox_funcs.h
  /usr/include/wfmath-0.3/wfmath/rotmatrix.h
  /usr/include/wfmath-0.3/wfmath/rotmatrix_funcs.h
  /usr/include/wfmath-0.3/wfmath/segment.h
  /usr/include/wfmath-0.3/wfmath/segment_funcs.h
  /usr/include/wfmath-0.3/wfmath/shuffle.h
  /usr/include/wfmath-0.3/wfmath/stream.h
  /usr/include/wfmath-0.3/wfmath/timestamp.h
  /usr/include/wfmath-0.3/wfmath/vector.h
  /usr/include/wfmath-0.3/wfmath/vector_funcs.h
  /usr/include/wfmath-0.3/wfmath/wfmath.h
  /usr/include/wfmath-0.3/wfmath/wrapped_array.h
  /usr/include/wfmath-0.3/wfmath/zero.h
  /usr/lib/libwfmath-0.3.dll.a
  /usr/lib/libwfmath-0.3.la
  /usr/lib/pkgconfig/wfmath-0.3.pc
  /usr/share/man/man3/WFMath.3.gz
  /usr/share/man/man3/WFMath_AxisBox.3.gz
  /usr/share/man/man3/WFMath_Ball.3.gz
  /usr/share/man/man3/WFMath_ColinearVectors.3.gz
  /usr/share/man/man3/WFMath_ParseError.3.gz
  /usr/share/man/man3/WFMath_Point.3.gz
  /usr/share/man/man3/WFMath_Polygon.3.gz
  /usr/share/man/man3/WFMath_Polygon_2.3.gz
  /usr/share/man/man3/WFMath_Quaternion.3.gz
  /usr/share/man/man3/WFMath_RotBox.3.gz
  /usr/share/man/man3/WFMath_RotMatrix.3.gz
  /usr/share/man/man3/WFMath_Segment.3.gz
  /usr/share/man/man3/WFMath_TimeDiff.3.gz
  /usr/share/man/man3/WFMath_TimeStamp.3.gz
  /usr/share/man/man3/WFMath_Vector.3.gz
  /usr/share/man/man3/WFMath_ZeroPrimitive.3.gz

------------------

Port Notes:

----- version 0.3.12-1bl1 -----
Version bump.

----- version 0.3.11-1bl1 -----
Version bump.

----- version 0.3.10-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

