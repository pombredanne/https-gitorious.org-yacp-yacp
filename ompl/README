ompl
------------------------------------------
The Open Motion Planning Library (OMPL) consists of many state-of-the-art
sampling-based motion planning algorithms. OMPL itself does not contain any
code related to, e.g., collision checking or visualization. This is a
deliberate design choice, so that OMPL is not tied to a particular collision
checker or visualization front end.

Runtime requirements:
  cygwin-1.7.9-1
  libboost1.48-1.48.0-1
  libgcc1-4.5.3-3
  libode1-0.11.1-1
  libstdc++6-4.5.3-3

Build requirements:
(besides corresponding -devel packages)
  autoconf-10-1
  automake-4-10
  binutils-2.22.51-1
  cygport-0.10.7-1
  gawk-4.0.0-1
  gcc4-core-4.5.3-3
  gcc4-g++-4.5.3-3
  libtool-2.4-1
  make-3.82.90-1

Canonical website:
  http://ompl.kavrakilab.org/

Canonical download:
  mirror://sourceforge/ompl/ompl-0.9.5-Source.tar.gz

-------------------------------------------

Build instructions:
  unpack ompl-0.9.5-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./ompl-0.9.5-X.cygport all

This will create:
  /usr/src/ompl-0.9.5-X-src.tar.bz2
  /usr/src/ompl-0.9.5-X.tar.bz2
  /usr/src/libompl1-0.9.5-X.tar.bz2
  /usr/src/libompl-devel-0.9.5-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(ompl)
  /usr/share/doc/Cygwin/ompl.README
  /usr/share/doc/ompl/ChangeLog
  /usr/share/doc/ompl/LICENSE
  /usr/share/doc/ompl/README.txt

(libompl1)
  /usr/bin/cygompl-1.dll

(libompl-devel)
  /usr/include/ompl/base/DiscreteMotionValidator.h
  /usr/include/ompl/base/Goal.h
  /usr/include/ompl/base/GoalLazySamples.h
  /usr/include/ompl/base/GoalRegion.h
  /usr/include/ompl/base/GoalSampleableRegion.h
  /usr/include/ompl/base/GoalState.h
  /usr/include/ompl/base/GoalStates.h
  /usr/include/ompl/base/GoalTypes.h
  /usr/include/ompl/base/MotionValidator.h
  /usr/include/ompl/base/Path.h
  /usr/include/ompl/base/Planner.h
  /usr/include/ompl/base/PlannerData.h
  /usr/include/ompl/base/PlannerTerminationCondition.h
  /usr/include/ompl/base/ProblemDefinition.h
  /usr/include/ompl/base/ProjectionEvaluator.h
  /usr/include/ompl/base/ScopedState.h
  /usr/include/ompl/base/SpaceInformation.h
  /usr/include/ompl/base/State.h
  /usr/include/ompl/base/StateSampler.h
  /usr/include/ompl/base/StateSamplerArray.h
  /usr/include/ompl/base/StateSpace.h
  /usr/include/ompl/base/StateSpaceTypes.h
  /usr/include/ompl/base/StateValidityChecker.h
  /usr/include/ompl/base/ValidStateSampler.h
  /usr/include/ompl/base/samplers/GaussianValidStateSampler.h
  /usr/include/ompl/base/samplers/MaximizeClearanceValidStateSampler.h
  /usr/include/ompl/base/samplers/ObstacleBasedValidStateSampler.h
  /usr/include/ompl/base/samplers/UniformValidStateSampler.h
  /usr/include/ompl/base/spaces/DiscreteStateSpace.h
  /usr/include/ompl/base/spaces/RealVectorBounds.h
  /usr/include/ompl/base/spaces/RealVectorStateProjections.h
  /usr/include/ompl/base/spaces/RealVectorStateSpace.h
  /usr/include/ompl/base/spaces/SE2StateSpace.h
  /usr/include/ompl/base/spaces/SE3StateSpace.h
  /usr/include/ompl/base/spaces/SO2StateSpace.h
  /usr/include/ompl/base/spaces/SO3StateSpace.h
  /usr/include/ompl/base/spaces/TimeStateSpace.h
  /usr/include/ompl/config.h
  /usr/include/ompl/contrib/rrt_star/BallTreeRRTstar.h
  /usr/include/ompl/contrib/rrt_star/RRTstar.h
  /usr/include/ompl/control/Control.h
  /usr/include/ompl/control/ControlSampler.h
  /usr/include/ompl/control/ControlSpace.h
  /usr/include/ompl/control/PathControl.h
  /usr/include/ompl/control/PlannerData.h
  /usr/include/ompl/control/SimpleSetup.h
  /usr/include/ompl/control/SpaceInformation.h
  /usr/include/ompl/control/StatePropagator.h
  /usr/include/ompl/control/planners/PlannerIncludes.h
  /usr/include/ompl/control/planners/kpiece/KPIECE1.h
  /usr/include/ompl/control/planners/rrt/RRT.h
  /usr/include/ompl/control/spaces/RealVectorControlSpace.h
  /usr/include/ompl/datastructures/BinaryHeap.h
  /usr/include/ompl/datastructures/GreedyKCenters.h
  /usr/include/ompl/datastructures/Grid.h
  /usr/include/ompl/datastructures/GridB.h
  /usr/include/ompl/datastructures/GridN.h
  /usr/include/ompl/datastructures/NearestNeighbors.h
  /usr/include/ompl/datastructures/NearestNeighborsGNAT.h
  /usr/include/ompl/datastructures/NearestNeighborsLinear.h
  /usr/include/ompl/datastructures/NearestNeighborsSqrtApprox.h
  /usr/include/ompl/datastructures/PDF.h
  /usr/include/ompl/extensions/ode/ODEControlSpace.h
  /usr/include/ompl/extensions/ode/ODEEnvironment.h
  /usr/include/ompl/extensions/ode/ODESimpleSetup.h
  /usr/include/ompl/extensions/ode/ODEStatePropagator.h
  /usr/include/ompl/extensions/ode/ODEStateSpace.h
  /usr/include/ompl/extensions/ode/ODEStateValidityChecker.h
  /usr/include/ompl/geometric/PathGeometric.h
  /usr/include/ompl/geometric/PathSimplifier.h
  /usr/include/ompl/geometric/SimpleSetup.h
  /usr/include/ompl/geometric/ik/GAIK.h
  /usr/include/ompl/geometric/ik/HCIK.h
  /usr/include/ompl/geometric/planners/PlannerIncludes.h
  /usr/include/ompl/geometric/planners/est/EST.h
  /usr/include/ompl/geometric/planners/kpiece/BKPIECE1.h
  /usr/include/ompl/geometric/planners/kpiece/Discretization.h
  /usr/include/ompl/geometric/planners/kpiece/KPIECE1.h
  /usr/include/ompl/geometric/planners/kpiece/LBKPIECE1.h
  /usr/include/ompl/geometric/planners/prm/ConnectionStrategy.h
  /usr/include/ompl/geometric/planners/prm/PRM.h
  /usr/include/ompl/geometric/planners/rrt/LazyRRT.h
  /usr/include/ompl/geometric/planners/rrt/RRT.h
  /usr/include/ompl/geometric/planners/rrt/RRTConnect.h
  /usr/include/ompl/geometric/planners/rrt/pRRT.h
  /usr/include/ompl/geometric/planners/sbl/SBL.h
  /usr/include/ompl/geometric/planners/sbl/pSBL.h
  /usr/include/ompl/tools/benchmark/Benchmark.h
  /usr/include/ompl/tools/benchmark/MachineSpecs.h
  /usr/include/ompl/tools/config/MagicConstants.h
  /usr/include/ompl/tools/config/SelfConfig.h
  /usr/include/ompl/tools/spaces/StateSpaceCollection.h
  /usr/include/ompl/util/ClassForward.h
  /usr/include/ompl/util/Console.h
  /usr/include/ompl/util/Exception.h
  /usr/include/ompl/util/Profiler.h
  /usr/include/ompl/util/RandomNumbers.h
  /usr/include/ompl/util/Time.h
  /usr/lib/libompl.dll.a
  /usr/share/ompl/demos/ODERigidBodyPlanning.cpp
  /usr/share/ompl/demos/RigidBodyPlanning.cpp
  /usr/share/ompl/demos/RigidBodyPlanningWithControls.cpp
  /usr/share/ompl/demos/RigidBodyPlanningWithIK.cpp
  /usr/share/ompl/demos/RigidBodyPlanningWithIntegrationAndControls.cpp
  /usr/share/ompl/demos/StateSampling.cpp

------------------

Port Notes:

----- version 0.9.5-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

