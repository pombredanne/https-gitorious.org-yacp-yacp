physfs
------------------------------------------
PhysicsFS is a library to provide abstract access to various archives. It is
intended for use in video games, and the design was somewhat inspired by Quake
3's file subsystem. The programmer defines a "write directory" on the physical
filesystem. No file writing done through the PhysicsFS API can leave that
write directory, for security. For example, an embedded scripting language
cannot write outside of this path if it uses PhysFS for all of its I/O, which
means that untrusted scripts can run more safely. Symbolic links can be
disabled as well, for added safety. For file reading, the programmer lists
directories and archives that form a "search path". Once the search path is
defined, it becomes a single, transparent hierarchical filesystem. This makes
for easy access to ZIP files in the same way as you access a file directly on
the disk, and it makes it easy to ship a new archive that will override a
previous archive on a per-file basis. Finally, PhysicsFS gives you
platform-abstracted means to determine if CD-ROMs are available, the user's
home directory, where in the real filesystem your program is running, etc.

Runtime requirements:
  cygwin-1.7.9-1
  libgcc1-4.5.3-1
  libreadline7-6.1.2-2
  zlib0-1.2.5-1

Build requirements:
(besides corresponding -devel packages)
  binutils-2.21-1
  cmake-2.8.4-1
  cygport-0.10.4-1
  gcc4-core-4.5.3-1
  make-3.81-2

Canonical website:
  http://icculus.org/physfs/

Canonical download:
  http://icculus.org/physfs/downloads/physfs-2.0.2.tar.gz

-------------------------------------------

Build instructions:
  unpack physfs-2.0.2-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./physfs-2.0.2-X.cygport all

This will create:
  /usr/src/physfs-2.0.2-X-src.tar.bz2
  /usr/src/physfs-2.0.2-X.tar.bz2
  /usr/src/libphysfs1-2.0.2-X.tar.bz2
  /usr/src/libphysfs-devel-2.0.2-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(physfs)
  /usr/bin/test_physfs.exe
  /usr/share/doc/Cygwin/physfs.README
  /usr/share/doc/physfs/CREDITS.txt
  /usr/share/doc/physfs/ChangeLog.txt
  /usr/share/doc/physfs/LICENSE.txt
  /usr/share/doc/physfs/TODO.txt

(libphysfs1)
  /usr/bin/cygphysfs-1.dll

(libphysfs-devel)
  /usr/include/physfs.h
  /usr/lib/libphysfs.dll.a

------------------

Port Notes:

----- version 2.0.2-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

