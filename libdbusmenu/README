libdbusmenu
------------------------------------------
A small little library that was created by pulling out some comon code out of
indicator-applet. It passes a menu structure across DBus so that a program can
create a menu simply without worrying about how it is displayed on the other
side of the bus.

Runtime requirements:
  cygwin-1.7.9-1
  libgdk_pixbuf2.0_0-2.22.1-1
  libglib2.0_0-2.28.3-1
  libgtk2.0_0-2.24.3-1
  libjson-glib1.0_0-0.12.2-1
  libpango1.0_0-1.28.3-2
  libX11_6-1.4.2-1

Build requirements:
(besides corresponding -devel packages)
  autoconf-10-1
  automake-4-10
  binutils-2.21-1
  cygport-0.10.4-1
  gawk-3.1.8-1
  gcc4-core-4.5.2-2
  libtool-2.4-1
  make-3.81-2
  vala-0.10.3-1

Canonical website:
  https://launchpad.net/dbusmenu

Canonical download:
  http://launchpad.net/dbusmenu/0.4/0.4.4/+download/libdbusmenu-0.4.4.tar.gz

-------------------------------------------

Build instructions:
  unpack libdbusmenu-0.4.4-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./libdbusmenu-0.4.4-X.cygport all

This will create:
  /usr/src/libdbusmenu-0.4.4-X-src.tar.bz2
  /usr/src/libdbusmenu-0.4.4-X.tar.bz2
  /usr/src/libdbusmenu-glib3-0.4.4-X.tar.bz2
  /usr/src/libdbusmenu-glib-devel-0.4.4-X.tar.bz2
  /usr/src/libdbusmenu-gtk3-0.4.4-X.tar.bz2
  /usr/src/libdbusmenu-gtk-devel-0.4.4-X.tar.bz2
  /usr/src/libdbusmenu-jsonloader3-0.4.4-X.tar.bz2
  /usr/src/libdbusmenu-jsonloader-devel-0.4.4-X.tar.bz2
  /usr/src/girepository-Dbusmenu0.4-0.4.4-x.tar.bz2
  /usr/src/girepository-DbusmenuGtk0.4-0.4.4-x.tar.bz2
  /usr/src/vala-dbusmenu0.4-0.4.4-x.tar.bz2
  /usr/src/vala-dbusmenugtk0.4-0.4.4-x.tar.bz2

-------------------------------------------

Files included in the binary package:

(libdbusmenu)
  /usr/lib/dbusmenu-bench
  /usr/lib/dbusmenu-dumper.exe
  /usr/lib/dbusmenu-testapp.exe
  /usr/share/doc/Cygwin/libdbusmenu.README
  /usr/share/doc/libdbusmenu/AUTHORS
  /usr/share/doc/libdbusmenu/COPYING
  /usr/share/doc/libdbusmenu/ChangeLog
  /usr/share/doc/libdbusmenu/README
  /usr/share/doc/libdbusmenu/README.dbusmenu-bench

(libdbusmenu-glib3)
  /usr/bin/cygdbusmenu-glib-3.dll

(libdbusmenu-glib-devel)
  /usr/include/libdbusmenu-0.4/libdbusmenu-glib/client.h
  /usr/include/libdbusmenu-0.4/libdbusmenu-glib/dbusmenu-glib.h
  /usr/include/libdbusmenu-0.4/libdbusmenu-glib/enum-types.h
  /usr/include/libdbusmenu-0.4/libdbusmenu-glib/menuitem-proxy.h
  /usr/include/libdbusmenu-0.4/libdbusmenu-glib/menuitem.h
  /usr/include/libdbusmenu-0.4/libdbusmenu-glib/server.h
  /usr/include/libdbusmenu-0.4/libdbusmenu-glib/types.h
  /usr/lib/libdbusmenu-glib.dll.a
  /usr/lib/libdbusmenu-glib.la
  /usr/lib/pkgconfig/dbusmenu-glib.pc
  /usr/share/doc/libdbusmenu/examples/glib-server-nomenu.c
  /usr/share/gtk-doc/html/libdbusmenu-glib/annotation-glossary.html
  /usr/share/gtk-doc/html/libdbusmenu-glib/api-index-deprecated.html
  /usr/share/gtk-doc/html/libdbusmenu-glib/api-index-full.html
  /usr/share/gtk-doc/html/libdbusmenu-glib/ch01.html
  /usr/share/gtk-doc/html/libdbusmenu-glib/home.png
  /usr/share/gtk-doc/html/libdbusmenu-glib/index.html
  /usr/share/gtk-doc/html/libdbusmenu-glib/index.sgml
  /usr/share/gtk-doc/html/libdbusmenu-glib/left.png
  /usr/share/gtk-doc/html/libdbusmenu-glib/libdbusmenu-glib-DbusmenuClient.html
  /usr/share/gtk-doc/html/libdbusmenu-glib/libdbusmenu-glib-DbusmenuMenuitem.html
  /usr/share/gtk-doc/html/libdbusmenu-glib/libdbusmenu-glib-DbusmenuMenuitemProxy.html
  /usr/share/gtk-doc/html/libdbusmenu-glib/libdbusmenu-glib-DbusmenuServer.html
  /usr/share/gtk-doc/html/libdbusmenu-glib/libdbusmenu-glib-Types.html
  /usr/share/gtk-doc/html/libdbusmenu-glib/libdbusmenu-glib.devhelp
  /usr/share/gtk-doc/html/libdbusmenu-glib/libdbusmenu-glib.devhelp2
  /usr/share/gtk-doc/html/libdbusmenu-glib/object-tree.html
  /usr/share/gtk-doc/html/libdbusmenu-glib/right.png
  /usr/share/gtk-doc/html/libdbusmenu-glib/style.css
  /usr/share/gtk-doc/html/libdbusmenu-glib/up.png

(libdbusmenu-gtk3)
  /usr/bin/cygdbusmenu-gtk-3.dll

(libdbusmenu-gtk-devel)
  /usr/include/libdbusmenu-0.4/libdbusmenu-gtk/client.h
  /usr/include/libdbusmenu-0.4/libdbusmenu-gtk/dbusmenu-gtk.h
  /usr/include/libdbusmenu-0.4/libdbusmenu-gtk/menu.h
  /usr/include/libdbusmenu-0.4/libdbusmenu-gtk/menuitem.h
  /usr/include/libdbusmenu-0.4/libdbusmenu-gtk/parser.h
  /usr/include/libdbusmenu-0.4/libdbusmenu-gtk/serializablemenuitem.h
  /usr/lib/libdbusmenu-gtk.dll.a
  /usr/lib/libdbusmenu-gtk.la
  /usr/lib/pkgconfig/dbusmenu-gtk.pc
  /usr/share/gtk-doc/html/libdbusmenu-gtk/annotation-glossary.html
  /usr/share/gtk-doc/html/libdbusmenu-gtk/api-index-deprecated.html
  /usr/share/gtk-doc/html/libdbusmenu-gtk/api-index-full.html
  /usr/share/gtk-doc/html/libdbusmenu-gtk/ch01.html
  /usr/share/gtk-doc/html/libdbusmenu-gtk/home.png
  /usr/share/gtk-doc/html/libdbusmenu-gtk/index.html
  /usr/share/gtk-doc/html/libdbusmenu-gtk/index.sgml
  /usr/share/gtk-doc/html/libdbusmenu-gtk/left.png
  /usr/share/gtk-doc/html/libdbusmenu-gtk/libdbusmenu-gtk-DbusmenuGtkClient.html
  /usr/share/gtk-doc/html/libdbusmenu-gtk/libdbusmenu-gtk-DbusmenuGtkMenu.html
  /usr/share/gtk-doc/html/libdbusmenu-gtk/libdbusmenu-gtk-menuitem.html
  /usr/share/gtk-doc/html/libdbusmenu-gtk/libdbusmenu-gtk-parser.html
  /usr/share/gtk-doc/html/libdbusmenu-gtk/libdbusmenu-gtk.devhelp
  /usr/share/gtk-doc/html/libdbusmenu-gtk/libdbusmenu-gtk.devhelp2
  /usr/share/gtk-doc/html/libdbusmenu-gtk/object-tree.html
  /usr/share/gtk-doc/html/libdbusmenu-gtk/right.png
  /usr/share/gtk-doc/html/libdbusmenu-gtk/style.css
  /usr/share/gtk-doc/html/libdbusmenu-gtk/up.png

(libdbusmenu-jsonloader3)
  /usr/bin/cygdbusmenu-jsonloader-3.dll

(libdbusmenu-jsonloader-devel)
  /usr/include/libdbusmenu-0.4/libdbusmenu-jsonloader/json-loader.h
  /usr/lib/libdbusmenu-jsonloader.dll.a
  /usr/lib/libdbusmenu-jsonloader.la
  /usr/lib/pkgconfig/dbusmenu-jsonloader.pc
  /usr/share/libdbusmenu/json/test-gtk-label.json

(girepository-Dbusmenu0.4)
  /usr/lib/girepository-1.0/Dbusmenu-0.4.typelib
  /usr/share/gir-1.0/Dbusmenu-0.4.gir

(girepository-DbusmenuGtk0.4)
  /usr/lib/girepository-1.0/DbusmenuGtk-0.4.typelib
  /usr/share/gir-1.0/DbusmenuGtk-0.4.gir

(vala-dbusmenu0.4)
  /usr/share/vala/vapi/Dbusmenu-0.4.vapi

(vala-dbusmenugtk0.4)
  /usr/share/vala/vapi/DbusmenuGtk-0.4.vapi

------------------

Port Notes:

----- version 0.4.4-1bl1 -----
Version bump.

----- version 0.3.98-1bl1 -----
Version bump.

----- version 0.3.90-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

