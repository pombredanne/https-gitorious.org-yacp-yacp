libindicate
------------------------------------------
A small library for applications to raise 'flags' on DBus for other components
of the desktop to pick up and visualize. Currently used by the messaging
indicator.

Runtime requirements:
  cygwin-1.7.9-1
  libgdk_pixbuf2.0_0-2.22.1-1
  libglib2.0_0-2.28.3-1
  mono-2.8.2-1
  mono-glib-sharp2.0-2.12.10-1
  mono-gtk-sharp2.0-2.12.10-1
  python-2.6.5-2
  python-gobject-2.28.3-1

Build requirements:
(besides corresponding -devel packages)
  autoconf-10-1
  automake-4-10
  binutils-2.21-1
  cygport-0.10.4-1
  gawk-3.1.8-1
  gcc4-core-4.5.2-2
  libtool-2.4-1
  make-3.81-2

Canonical website:
  https://launchpad.net/libindicate/

Canonical download:
  http://launchpad.net/libindicate/0.5/0.5.0/+download/libindicate-0.5.0.tar.gz

-------------------------------------------

Build instructions:
  unpack libindicate-0.5.0-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./libindicate-0.5.0-X.cygport all

This will create:
  /usr/src/libindicate-0.5.0-X-src.tar.bz2
  /usr/src/libindicate-0.5.0-X.tar.bz2
  /usr/src/libindicate4-0.5.0-X.tar.bz2
  /usr/src/libindicate-devel-0.5.0-X.tar.bz2
  /usr/src/libindicate-gtk2-0.5.0-X.tar.bz2
  /usr/src/libindicate-gtk-devel-0.5.0-X.tar.bz2
  /usr/src/python-indicate-0.5.0-X.tar.bz2
  /usr/src/girepository-Indicate0.5-0.5.0-X.tar.bz2
  /usr/src/girepository-Indicate-Gtk0.5-0.5.0-X.tar.bz2
  /usr/src/vala-indicate0.5-0.5.0-X.tar.bz2
  /usr/src/vala-indicate-gtk0.5-0.5.0-X.tar.bz2
  /usr/src/mono-indicate-sharp0.1-0.5.0-X.tar.bz2
  /usr/src/mono-indicate-gtk-sharp0.1-0.5.0-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(libindicate)
  /usr/share/doc/Cygwin/libindicate.README
  /usr/share/doc/libindicate/AUTHORS
  /usr/share/doc/libindicate/COPYING
  /usr/share/doc/libindicate/ChangeLog

(libindicate5)
  /usr/bin/cygindicate-5.dll

(libindicate-devel)
  /usr/include/libindicate-0.5/libindicate/indicate-enum-types.h
  /usr/include/libindicate-0.5/libindicate/indicator-messages.h
  /usr/include/libindicate-0.5/libindicate/indicator.h
  /usr/include/libindicate-0.5/libindicate/interests.h
  /usr/include/libindicate-0.5/libindicate/listener.h
  /usr/include/libindicate-0.5/libindicate/server.h
  /usr/lib/libindicate.dll.a
  /usr/lib/libindicate.la
  /usr/lib/pkgconfig/indicate-0.5.pc
  /usr/share/doc/libindicate/examples/im-client.c
  /usr/share/doc/libindicate/examples/indicate-alot.c
  /usr/share/doc/libindicate/examples/indicate-and-crash.c
  /usr/share/doc/libindicate/examples/listen-and-print.c
  /usr/share/gtk-doc/html/libindicate/IndicateIndicator.html
  /usr/share/gtk-doc/html/libindicate/IndicateListener.html
  /usr/share/gtk-doc/html/libindicate/IndicateServer.html
  /usr/share/gtk-doc/html/libindicate/base.html
  /usr/share/gtk-doc/html/libindicate/home.png
  /usr/share/gtk-doc/html/libindicate/index.html
  /usr/share/gtk-doc/html/libindicate/index.sgml
  /usr/share/gtk-doc/html/libindicate/ix01.html
  /usr/share/gtk-doc/html/libindicate/left.png
  /usr/share/gtk-doc/html/libindicate/libindicate.devhelp
  /usr/share/gtk-doc/html/libindicate/libindicate.devhelp2
  /usr/share/gtk-doc/html/libindicate/listeners.html
  /usr/share/gtk-doc/html/libindicate/right.png
  /usr/share/gtk-doc/html/libindicate/style.css
  /usr/share/gtk-doc/html/libindicate/subclass.html
  /usr/share/gtk-doc/html/libindicate/up.png

(libindicate-gtk2)
  /usr/bin/cygindicate-gtk-2.dll

(libindicate-gtk-devel)
  /usr/include/libindicate-0.5/libindicate-gtk/indicator.h
  /usr/include/libindicate-0.5/libindicate-gtk/listener.h
  /usr/lib/libindicate-gtk.dll.a
  /usr/lib/libindicate-gtk.la
  /usr/lib/pkgconfig/indicate-gtk-0.5.pc

(python-indicate)
  /usr/lib/python2.6/site-packages/indicate/__init__.py
  /usr/lib/python2.6/site-packages/indicate/__init__.pyc
  /usr/lib/python2.6/site-packages/indicate/__init__.pyo
  /usr/lib/python2.6/site-packages/indicate/_indicate.dll
  /usr/lib/python2.6/site-packages/indicate/_indicate.la
  /usr/share/doc/libindicate/examples/im-client.py
  /usr/share/doc/libindicate/examples/listen-and-print.py
  /usr/share/pygtk/2.0/defs/indicate.defs

(girepository-Indicate0.5)
  /usr/lib/girepository-1.0/Indicate-0.5.typelib
  /usr/share/gir-1.0/Indicate-0.5.gir

(girepository-Indicate-Gtk0.5)
  /usr/lib/girepository-1.0/Indicate-Gtk-0.5.typelib
  /usr/share/gir-1.0/Indicate-Gtk-0.5.gir

(vala-indicate0.5)
  /usr/share/vala/vapi/Indicate-0.5.vapi

(vala-indicate-gtk0.5)
  /usr/share/vala/vapi/Indicate-Gtk-0.5.vapi

(mono-indicate-sharp0.1)
  /usr/lib/indicate-sharp-0.1/indicate-sharp.dll
  /usr/lib/indicate-sharp-0.1/indicate-sharp.dll.config
  /usr/lib/mono/gac/indicate-sharp/0.4.1.0__2e8e49ba7d172cb0/indicate-sharp.dll
  /usr/lib/mono/gac/indicate-sharp/0.4.1.0__2e8e49ba7d172cb0/indicate-sharp.dll.config
  /usr/lib/mono/indicate/indicate-sharp.dll
  /usr/lib/pkgconfig/indicate-sharp-0.1.pc

(mono-indicate-gtk-sharp0.1)
  /usr/lib/indicate-gtk-sharp-0.1/indicate-gtk-sharp.dll
  /usr/lib/indicate-gtk-sharp-0.1/indicate-gtk-sharp.dll.config
  /usr/lib/mono/gac/indicate-gtk-sharp/0.5.0.0__fc29729774d50df1/indicate-gtk-sharp.dll
  /usr/lib/mono/gac/indicate-gtk-sharp/0.5.0.0__fc29729774d50df1/indicate-gtk-sharp.dll.config
  /usr/lib/mono/indicate-gtk/indicate-gtk-sharp.dll
  /usr/lib/pkgconfig/indicate-gtk-sharp-0.1.pc

------------------

Port Notes:

----- version 0.5.0-1bl1 -----
Version bump.

----- version 0.3.6-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

