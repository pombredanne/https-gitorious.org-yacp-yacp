libconfig
------------------------------------------
Libconfig is a simple library for processing structured configuration files,
like this one: test.cfg. This file format is more compact and more readable
than XML. And unlike XML, it is type-aware, so it is not necessary to do string
parsing in application code.

Runtime requirements:
  cygwin-1.7.9-1
  libgcc1-4.5.2-2
  libstdc++6-4.5.2-2

Build requirements:
(besides corresponding -devel packages)
  autoconf-10-1
  automake-4-10
  binutils-2.21-1
  cygport-0.10.4-1
  gawk-3.1.8-1
  gcc4-core-4.5.2-2
  gcc4-g++-4.5.2-2
  libtool-2.4-1
  make-3.81-2

Canonical website:
  http://www.hyperrealm.com/libconfig/

Canonical download:
  http://www.hyperrealm.com/libconfig/libconfig-1.4.7.tar.gz

-------------------------------------------

Build instructions:
  unpack libconfig-1.4.7-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./libconfig-1.4.7-X.cygport all

This will create:
  /usr/src/libconfig-1.4.7-X-src.tar.bz2
  /usr/src/libconfig-1.4.7-X.tar.bz2
  /usr/src/libconfig9-1.4.7-X.tar.bz2
  /usr/src/libconfig++9-1.4.7-X.tar.bz2
  /usr/src/libconfig-devel-1.4.7-X.tar.bz2
  /usr/src/libconfig++-devel-1.4.7-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(libconfig)
  /usr/bin/libconfig_tests.exe
  /usr/share/doc/Cygwin/libconfig.README
  /usr/share/doc/libconfig/AUTHORS
  /usr/share/doc/libconfig/ChangeLog
  /usr/share/doc/libconfig/COPYING.LIB
  /usr/share/doc/libconfig/README
  /usr/share/doc/libconfig/test.cfg
  /usr/share/doc/libconfig/TODO

(libconfig9)
  /usr/bin/cygconfig-9.dll

(libconfig++9)
  /usr/bin/cygconfig++-9.dll

(libconfig-devel)
  /usr/include/libconfig.h
  /usr/lib/libconfig.dll.a
  /usr/lib/libconfig.la
  /usr/lib/pkgconfig/libconfig.pc
  /usr/share/info/libconfig.info.gz

(libconfig++-devel)
  /usr/include/libconfig.h++
  /usr/lib/libconfig++.dll.a
  /usr/lib/libconfig++.la
  /usr/lib/pkgconfig/libconfig++.pc

------------------

Port Notes:

----- version 1.4.7-1bl1 -----
Version bump.

----- version 1.4.5-1bl1 -----
Version bump.

----- version 1.4.4-1bl1 -----
Version bump.

----- version 1.4b4-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

