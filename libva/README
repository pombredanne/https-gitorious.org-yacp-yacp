libva
------------------------------------------
Video Acceleration API (VA API) is a library (libVA) and API specification
which enables and provides access to graphics hardware (GPU) acceleration for
video processing on Linux and UNIX based operating systems. Accelerated
processing includes video decoding, video encoding, subpicture blending and
rendering. The specification was originally designed by Intel for its GMA
(Graphics Media Accelerator) series of GPU hardware, the API is however not
limited to GPUs or Intel specific hardware, as other hardware and
manufacturers can also freely use this API for hardware accelerated video
decoding.

Runtime requirements:
  cygwin-1.7.11-1
  libdrm2-2.4.25-1bl1
  libGL1-7.11.2-1
  libX11_6-1.4.4-1
  libXext6-1.3.0-1
  libXfixes3-5.0-1

Build requirements:
(besides corresponding -devel packages)
  autoconf-10-1
  automake-4-10
  binutils-2.22.51-1
  cygport-0.10.9-1
  gawk-4.0.0-1
  gcc4-core-4.5.3-3
  libtool-2.4-1
  make-3.82.90-1

Canonical website:
  http://freedesktop.org/wiki/Software/vaapi

Canonical download:
  http://cgit.freedesktop.org/libva/snapshot/libva-1.0.15.tar.bz2

-------------------------------------------

Build instructions:
  unpack libva-1.0.15-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./libva-1.0.15-X.cygport all

This will create:
  /usr/src/libva-1.0.15-X-src.tar.bz2
  /usr/src/libva-1.0.15-X.tar.bz2
  /usr/src/libva1-1.0.15-X.tar.bz2
  /usr/src/libva-devel-1.0.15-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(libva)
  /usr/bin/avcenc.exe
  /usr/bin/h264encode.exe
  /usr/bin/mpeg2vldemo.exe
  /usr/bin/putsurface.exe
  /usr/bin/vainfo.exe
  /usr/share/doc/Cygwin/libva.README
  /usr/share/doc/libva/COPYING
  /usr/share/doc/libva/NEWS

(libva1)
  /usr/bin/cygva-1.dll
  /usr/bin/cygva-egl-1.dll
  /usr/bin/cygva-glx-1.dll
  /usr/bin/cygva-tpi-1.dll
  /usr/bin/cygva-x11-1.dll
  /usr/lib/dri/dummy_drv_video.dll

(libva-devel)
  /usr/include/va/va.h
  /usr/include/va/va_backend.h
  /usr/include/va/va_backend_egl.h
  /usr/include/va/va_backend_glx.h
  /usr/include/va/va_backend_tpi.h
  /usr/include/va/va_dri.h
  /usr/include/va/va_dri2.h
  /usr/include/va/va_dricommon.h
  /usr/include/va/va_dummy.h
  /usr/include/va/va_egl.h
  /usr/include/va/va_glx.h
  /usr/include/va/va_tpi.h
  /usr/include/va/va_version.h
  /usr/include/va/va_x11.h
  /usr/lib/libva-egl.dll.a
  /usr/lib/libva-egl.la
  /usr/lib/libva-glx.dll.a
  /usr/lib/libva-glx.la
  /usr/lib/libva-tpi.dll.a
  /usr/lib/libva-tpi.la
  /usr/lib/libva-x11.dll.a
  /usr/lib/libva-x11.la
  /usr/lib/libva.dll.a
  /usr/lib/libva.la
  /usr/lib/pkgconfig/libva-egl.pc
  /usr/lib/pkgconfig/libva-glx.pc
  /usr/lib/pkgconfig/libva-tpi.pc
  /usr/lib/pkgconfig/libva-x11.pc
  /usr/lib/pkgconfig/libva.pc

------------------

Port Notes:

----- version 1.0.15-1bl1 -----
Version bump.

----- version 1.0.14-1bl1 -----
Version bump.

----- version 1.0.13-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

