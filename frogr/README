frogr
------------------------------------------
Frogr is a small application for the GNOME desktop that allows users to manage
their accounts in the Flickr image hosting website. It supports all the basic
Flickr features, including uploading pictures, adding descriptions, setting
tags and managing sets and groups pools.

Runtime requirements:
  cygwin-1.7.9-1
  libexif12-0.6.20-1
  libgdk_pixbuf2.0_0-2.23.5-1
  libglib2.0_0-2.28.8-1
  libgtk3_0-3.0.12-1
  libintl8-0.17-11
  libsoup-gnome2.4_1-2.34.3-1
  libsoup2.4_1-2.34.3-1
  libxml2-2.7.8-2

Build requirements:
(besides corresponding -devel packages)
  autoconf-10-1
  automake-4-10
  binutils-2.21.53-2
  cygport-0.10.4-1
  gawk-3.1.8-1
  gcc4-core-4.5.3-1
  libtool-2.4-1
  make-3.81-2

Canonical website:
  http://live.gnome.org/Frogr

Canonical download:
  mirror://gnome/sources/frogr/0.6/frogr-0.6.1.tar.bz2

-------------------------------------------

Build instructions:
  unpack frogr-0.6.1-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./frogr-0.6.1-X.cygport all

This will create:
  /usr/src/frogr-0.6.1-X-src.tar.bz2
  /usr/src/frogr-0.6.1-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(frogr)
  /etc/postinstall/frogr.sh
  /usr/bin/frogr.exe
  /usr/share/applications/frogr.desktop
  /usr/share/doc/Cygwin/frogr.README
  /usr/share/doc/frogr/AUTHORS
  /usr/share/doc/frogr/COPYING
  /usr/share/doc/frogr/ChangeLog
  /usr/share/doc/frogr/MAINTAINERS
  /usr/share/doc/frogr/NEWS
  /usr/share/doc/frogr/README
  /usr/share/doc/frogr/THANKS
  /usr/share/doc/frogr/TODO
  /usr/share/frogr/gtkbuilder/frogr-main-view.xml
  /usr/share/frogr/images/mpictures.png
  /usr/share/gnome/help/frogr/C/add-pictures.page
  /usr/share/gnome/help/frogr/C/create-account.page
  /usr/share/gnome/help/frogr/C/display.page
  /usr/share/gnome/help/frogr/C/edit-pictures.page
  /usr/share/gnome/help/frogr/C/external-viewer.page
  /usr/share/gnome/help/frogr/C/figures/edit-picture-details.png
  /usr/share/gnome/help/frogr/C/figures/frogr-remote-organizer.png
  /usr/share/gnome/help/frogr/C/figures/preferences-connection.png
  /usr/share/gnome/help/frogr/C/figures/preferences-general.png
  /usr/share/gnome/help/frogr/C/figures/preferences-misc.png
  /usr/share/gnome/help/frogr/C/group-pictures.page
  /usr/share/gnome/help/frogr/C/index.page
  /usr/share/gnome/help/frogr/C/introduction.page
  /usr/share/gnome/help/frogr/C/organize.page
  /usr/share/gnome/help/frogr/C/pictures.page
  /usr/share/gnome/help/frogr/C/preferences.page
  /usr/share/gnome/help/frogr/C/remove-pictures.page
  /usr/share/gnome/help/frogr/C/set-pictures.page
  /usr/share/gnome/help/frogr/C/tag.page
  /usr/share/gnome/help/frogr/C/upload-pictures.page
  /usr/share/gnome/help/frogr/cs/add-pictures.page
  /usr/share/gnome/help/frogr/cs/create-account.page
  /usr/share/gnome/help/frogr/cs/display.page
  /usr/share/gnome/help/frogr/cs/edit-pictures.page
  /usr/share/gnome/help/frogr/cs/external-viewer.page
  /usr/share/gnome/help/frogr/cs/figures/edit-picture-details.png
  /usr/share/gnome/help/frogr/cs/figures/frogr-remote-organizer.png
  /usr/share/gnome/help/frogr/cs/figures/preferences-connection.png
  /usr/share/gnome/help/frogr/cs/figures/preferences-general.png
  /usr/share/gnome/help/frogr/cs/figures/preferences-misc.png
  /usr/share/gnome/help/frogr/cs/group-pictures.page
  /usr/share/gnome/help/frogr/cs/index.page
  /usr/share/gnome/help/frogr/cs/introduction.page
  /usr/share/gnome/help/frogr/cs/organize.page
  /usr/share/gnome/help/frogr/cs/pictures.page
  /usr/share/gnome/help/frogr/cs/preferences.page
  /usr/share/gnome/help/frogr/cs/remove-pictures.page
  /usr/share/gnome/help/frogr/cs/set-pictures.page
  /usr/share/gnome/help/frogr/cs/tag.page
  /usr/share/gnome/help/frogr/cs/upload-pictures.page
  /usr/share/gnome/help/frogr/de/add-pictures.page
  /usr/share/gnome/help/frogr/de/create-account.page
  /usr/share/gnome/help/frogr/de/display.page
  /usr/share/gnome/help/frogr/de/edit-pictures.page
  /usr/share/gnome/help/frogr/de/external-viewer.page
  /usr/share/gnome/help/frogr/de/figures/edit-picture-details.png
  /usr/share/gnome/help/frogr/de/figures/frogr-remote-organizer.png
  /usr/share/gnome/help/frogr/de/figures/preferences-connection.png
  /usr/share/gnome/help/frogr/de/figures/preferences-general.png
  /usr/share/gnome/help/frogr/de/figures/preferences-misc.png
  /usr/share/gnome/help/frogr/de/group-pictures.page
  /usr/share/gnome/help/frogr/de/index.page
  /usr/share/gnome/help/frogr/de/introduction.page
  /usr/share/gnome/help/frogr/de/organize.page
  /usr/share/gnome/help/frogr/de/pictures.page
  /usr/share/gnome/help/frogr/de/preferences.page
  /usr/share/gnome/help/frogr/de/remove-pictures.page
  /usr/share/gnome/help/frogr/de/set-pictures.page
  /usr/share/gnome/help/frogr/de/tag.page
  /usr/share/gnome/help/frogr/de/upload-pictures.page
  /usr/share/gnome/help/frogr/en_GB/add-pictures.page
  /usr/share/gnome/help/frogr/en_GB/create-account.page
  /usr/share/gnome/help/frogr/en_GB/display.page
  /usr/share/gnome/help/frogr/en_GB/edit-pictures.page
  /usr/share/gnome/help/frogr/en_GB/external-viewer.page
  /usr/share/gnome/help/frogr/en_GB/figures/edit-picture-details.png
  /usr/share/gnome/help/frogr/en_GB/figures/frogr-remote-organizer.png
  /usr/share/gnome/help/frogr/en_GB/figures/preferences-connection.png
  /usr/share/gnome/help/frogr/en_GB/figures/preferences-general.png
  /usr/share/gnome/help/frogr/en_GB/figures/preferences-misc.png
  /usr/share/gnome/help/frogr/en_GB/group-pictures.page
  /usr/share/gnome/help/frogr/en_GB/index.page
  /usr/share/gnome/help/frogr/en_GB/introduction.page
  /usr/share/gnome/help/frogr/en_GB/organize.page
  /usr/share/gnome/help/frogr/en_GB/pictures.page
  /usr/share/gnome/help/frogr/en_GB/preferences.page
  /usr/share/gnome/help/frogr/en_GB/remove-pictures.page
  /usr/share/gnome/help/frogr/en_GB/set-pictures.page
  /usr/share/gnome/help/frogr/en_GB/tag.page
  /usr/share/gnome/help/frogr/en_GB/upload-pictures.page
  /usr/share/gnome/help/frogr/es/add-pictures.page
  /usr/share/gnome/help/frogr/es/create-account.page
  /usr/share/gnome/help/frogr/es/display.page
  /usr/share/gnome/help/frogr/es/edit-pictures.page
  /usr/share/gnome/help/frogr/es/external-viewer.page
  /usr/share/gnome/help/frogr/es/figures/edit-picture-details.png
  /usr/share/gnome/help/frogr/es/figures/frogr-remote-organizer.png
  /usr/share/gnome/help/frogr/es/figures/preferences-connection.png
  /usr/share/gnome/help/frogr/es/figures/preferences-general.png
  /usr/share/gnome/help/frogr/es/figures/preferences-misc.png
  /usr/share/gnome/help/frogr/es/group-pictures.page
  /usr/share/gnome/help/frogr/es/index.page
  /usr/share/gnome/help/frogr/es/introduction.page
  /usr/share/gnome/help/frogr/es/organize.page
  /usr/share/gnome/help/frogr/es/pictures.page
  /usr/share/gnome/help/frogr/es/preferences.page
  /usr/share/gnome/help/frogr/es/remove-pictures.page
  /usr/share/gnome/help/frogr/es/set-pictures.page
  /usr/share/gnome/help/frogr/es/tag.page
  /usr/share/gnome/help/frogr/es/upload-pictures.page
  /usr/share/gnome/help/frogr/fr/add-pictures.page
  /usr/share/gnome/help/frogr/fr/create-account.page
  /usr/share/gnome/help/frogr/fr/display.page
  /usr/share/gnome/help/frogr/fr/edit-pictures.page
  /usr/share/gnome/help/frogr/fr/external-viewer.page
  /usr/share/gnome/help/frogr/fr/figures/edit-picture-details.png
  /usr/share/gnome/help/frogr/fr/figures/frogr-remote-organizer.png
  /usr/share/gnome/help/frogr/fr/figures/preferences-connection.png
  /usr/share/gnome/help/frogr/fr/figures/preferences-general.png
  /usr/share/gnome/help/frogr/fr/figures/preferences-misc.png
  /usr/share/gnome/help/frogr/fr/group-pictures.page
  /usr/share/gnome/help/frogr/fr/index.page
  /usr/share/gnome/help/frogr/fr/introduction.page
  /usr/share/gnome/help/frogr/fr/organize.page
  /usr/share/gnome/help/frogr/fr/pictures.page
  /usr/share/gnome/help/frogr/fr/preferences.page
  /usr/share/gnome/help/frogr/fr/remove-pictures.page
  /usr/share/gnome/help/frogr/fr/set-pictures.page
  /usr/share/gnome/help/frogr/fr/tag.page
  /usr/share/gnome/help/frogr/fr/upload-pictures.page
  /usr/share/gnome/help/frogr/ru/add-pictures.page
  /usr/share/gnome/help/frogr/ru/create-account.page
  /usr/share/gnome/help/frogr/ru/display.page
  /usr/share/gnome/help/frogr/ru/edit-pictures.page
  /usr/share/gnome/help/frogr/ru/external-viewer.page
  /usr/share/gnome/help/frogr/ru/figures/edit-picture-details.png
  /usr/share/gnome/help/frogr/ru/figures/frogr-remote-organizer.png
  /usr/share/gnome/help/frogr/ru/figures/preferences-connection.png
  /usr/share/gnome/help/frogr/ru/figures/preferences-general.png
  /usr/share/gnome/help/frogr/ru/figures/preferences-misc.png
  /usr/share/gnome/help/frogr/ru/group-pictures.page
  /usr/share/gnome/help/frogr/ru/index.page
  /usr/share/gnome/help/frogr/ru/introduction.page
  /usr/share/gnome/help/frogr/ru/organize.page
  /usr/share/gnome/help/frogr/ru/pictures.page
  /usr/share/gnome/help/frogr/ru/preferences.page
  /usr/share/gnome/help/frogr/ru/remove-pictures.page
  /usr/share/gnome/help/frogr/ru/set-pictures.page
  /usr/share/gnome/help/frogr/ru/tag.page
  /usr/share/gnome/help/frogr/ru/upload-pictures.page
  /usr/share/gnome/help/frogr/sl/add-pictures.page
  /usr/share/gnome/help/frogr/sl/create-account.page
  /usr/share/gnome/help/frogr/sl/display.page
  /usr/share/gnome/help/frogr/sl/edit-pictures.page
  /usr/share/gnome/help/frogr/sl/external-viewer.page
  /usr/share/gnome/help/frogr/sl/figures/edit-picture-details.png
  /usr/share/gnome/help/frogr/sl/figures/frogr-remote-organizer.png
  /usr/share/gnome/help/frogr/sl/figures/preferences-connection.png
  /usr/share/gnome/help/frogr/sl/figures/preferences-general.png
  /usr/share/gnome/help/frogr/sl/figures/preferences-misc.png
  /usr/share/gnome/help/frogr/sl/group-pictures.page
  /usr/share/gnome/help/frogr/sl/index.page
  /usr/share/gnome/help/frogr/sl/introduction.page
  /usr/share/gnome/help/frogr/sl/organize.page
  /usr/share/gnome/help/frogr/sl/pictures.page
  /usr/share/gnome/help/frogr/sl/preferences.page
  /usr/share/gnome/help/frogr/sl/remove-pictures.page
  /usr/share/gnome/help/frogr/sl/set-pictures.page
  /usr/share/gnome/help/frogr/sl/tag.page
  /usr/share/gnome/help/frogr/sl/upload-pictures.page
  /usr/share/icons/hicolor/128x128/apps/frogr.png
  /usr/share/icons/hicolor/16x16/apps/frogr.png
  /usr/share/icons/hicolor/24x24/apps/frogr.png
  /usr/share/icons/hicolor/32x32/apps/frogr.png
  /usr/share/icons/hicolor/48x48/apps/frogr.png
  /usr/share/icons/hicolor/64x64/apps/frogr.png
  /usr/share/icons/hicolor/scalable/apps/frogr.svg
  /usr/share/locale/cs/LC_MESSAGES/frogr.mo
  /usr/share/locale/da/LC_MESSAGES/frogr.mo
  /usr/share/locale/de/LC_MESSAGES/frogr.mo
  /usr/share/locale/el/LC_MESSAGES/frogr.mo
  /usr/share/locale/en_GB/LC_MESSAGES/frogr.mo
  /usr/share/locale/es/LC_MESSAGES/frogr.mo
  /usr/share/locale/fr/LC_MESSAGES/frogr.mo
  /usr/share/locale/gl/LC_MESSAGES/frogr.mo
  /usr/share/locale/it/LC_MESSAGES/frogr.mo
  /usr/share/locale/pl/LC_MESSAGES/frogr.mo
  /usr/share/locale/pt_BR/LC_MESSAGES/frogr.mo
  /usr/share/locale/ru/LC_MESSAGES/frogr.mo
  /usr/share/locale/sl/LC_MESSAGES/frogr.mo
  /usr/share/locale/sv/LC_MESSAGES/frogr.mo
  /usr/share/locale/tr/LC_MESSAGES/frogr.mo
  /usr/share/locale/uk/LC_MESSAGES/frogr.mo
  /usr/share/locale/zh_CN/LC_MESSAGES/frogr.mo
  /usr/share/man/man1/frogr.1.gz
  /usr/share/pixmaps/frogr.xpm

------------------

Port Notes:

----- version 0.6.1-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

