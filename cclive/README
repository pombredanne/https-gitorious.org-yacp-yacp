cclive
------------------------------------------
cclive is a tool for downloading media from YouTube and similar websites. It
has a low memory footprint compared to other existing tools.

Runtime requirements:
  cygwin-1.7.11-1
  libboost1.48-1.48.0-1
  libcurl4-7.24.0-2
  libgcc1-4.5.3-3
  libpcrecpp0-8.21-2
  libquvi7-0.4.0-1
  libstdc++6-4.5.3-3

Build requirements:
(besides corresponding -devel packages)
  autoconf-10-1
  automake-4-10
  binutils-2.22.51-1
  cygport-0.10.9-1
  gawk-4.0.0-1
  gcc4-core-4.5.3-3
  gcc4-g++-4.5.3-3
  libtool-2.4-1
  make-3.82.90-1

Canonical website:
  http://cclive.sf.net/

Canonical download:
  mirror://sourceforge/cclive/cclive-0.7.9.tar.xz

-------------------------------------------

Build instructions:
  unpack cclive-0.7.9-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./cclive-0.7.9-X.cygport all

This will create:
  /usr/src/cclive-0.7.9-X-src.tar.bz2
  /usr/src/cclive-0.7.9-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(cclive)
  /usr/bin/cclive.exe
  /usr/share/doc/Cygwin/cclive.README
  /usr/share/doc/cclive/COPYING
  /usr/share/doc/cclive/ChangeLog
  /usr/share/doc/cclive/NEWS
  /usr/share/doc/cclive/README
  /usr/share/man/man1/cclive.1.gz

------------------

Port Notes:

----- version 0.7.9-1bl1 -----
Version bump.

----- version 0.7.8-1bl1 -----
Version bump.

----- version 0.6.6.20101007git-1bl1 -----
Version bump.

----- version 0.6.2-1bl1 -----
Version bump.

----- version 0.5.8-1bl1 -----
Version bump.

----- version 0.5.4-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

