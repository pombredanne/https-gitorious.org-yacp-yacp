airsched
------------------------------------------
airsched aims at providing a clean API and a simple implementation, as a C++
library, of an Airline Schedule Management System. It is intended to be used
in simulated environments only: it is not designed to work in the real-world
of Airline IT operations.

airsched makes an extensive use of existing open-source libraries for
increased functionality, speed and accuracy. In particular the Boost (C++
Standard Extensions: http://www.boost.org) library is used.

Install the airsched package if you need a library of basic C++ objects for
Airline Schedule Management, mainly for simulation purpose.

Runtime requirements:
  cygwin-1.7.12-1
  libboost1.48-1.48.0-1
  libgcc1-4.5.3-3
  libstdair0.45-0.45.1-1bl1
  libstdc++6-4.5.3-3

Build requirements:
(besides corresponding -devel packages)
  binutils-2.22.51-2
  cmake-2.8.4-1
  cygport-0.10.10-1
  gcc4-core-4.5.3-3
  gcc4-g++-4.5.3-3
  make-3.82.90-1

Canonical website:
  http://air-sched.sf.net/

Canonical download:
  mirror://sourceforge/air-sched/airsched-0.1.4.tar.bz2

-------------------------------------------

Build instructions:
  unpack airsched-0.1.4-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./airsched-0.1.4-X.cygport all

This will create:
  /usr/src/airsched-0.1.4-X-src.tar.bz2
  /usr/src/airsched-0.1.4-X.tar.bz2
  /usr/src/libairsched0.1-0.1.4-X.tar.bz2
  /usr/src/libairsched-devel-0.1.4-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(airsched)
  /usr/bin/airsched.exe
  /usr/share/doc/Cygwin/airsched.README
  /usr/share/doc/airsched/AUTHORS
  /usr/share/doc/airsched/COPYING
  /usr/share/doc/airsched/ChangeLog
  /usr/share/doc/airsched/NEWS
  /usr/share/doc/airsched/README
  /usr/share/man/man1/airsched.1.gz

(libairsched0.1)
  /usr/bin/cygairsched-0.1.dll

(libairsched-devel)
  /usr/bin/airsched-config
  /usr/include/airsched/AIRSCHED_Service.hpp
  /usr/include/airsched/AIRSCHED_Types.hpp
  /usr/lib/libairsched.dll.a
  /usr/lib/pkgconfig/airsched.pc
  /usr/share/aclocal/airsched.m4
  /usr/share/airsched/CMake/airsched-config-version.cmake
  /usr/share/airsched/CMake/airsched-config.cmake
  /usr/share/airsched/CMake/airsched-library-depends-release.cmake
  /usr/share/airsched/CMake/airsched-library-depends.cmake
  /usr/share/man/man1/airsched-config.1.gz
  /usr/share/man/man3/airsched-library.3.gz

------------------

Port Notes:

----- version 0.1.4-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

