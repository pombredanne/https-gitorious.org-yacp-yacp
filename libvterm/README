libvterm
------------------------------------------
Based on ROTE, libvterm is a terminal emulator library which attempts to mimic
both VT100 and rxvt capabilities. Although the natural display apparatus is
curses, the API is simple enough to grow and adopt other mechanisms.

Runtime requirements:
  cygwin-1.7.7-1
  libgcc1-4.3.4-3
  libglib2.0_0-2.24.1-1
  libncurses10-5.7-18

Build requirements:
(besides corresponding -devel packages)
  autoconf-9-1
  automake-4-10
  binutils-2.20.51-2
  cygport-0.10.2-1
  gawk-3.1.8-1
  gcc4-core-4.3.4-3
  libtool-2.4-1
  make-3.81-2

Canonical website:
  http://libvterm.sourceforge.net/

Canonical download:
  mirror://sourceforge/libvterm/libvterm-0.99.7.tar.gz

-------------------------------------------

Build instructions:
  unpack libvterm-0.99.7-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./libvterm-0.99.7-X.cygport all

This will create:
  /usr/src/libvterm-0.99.7-X-src.tar.bz2
  /usr/src/libvterm-0.99.7-X.tar.bz2
  /usr/src/libvterm0-0.99.7-X.tar.bz2
  /usr/src/libvterm-devel-0.99.7-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(libvterm)
  /usr/share/doc/Cygwin/libvterm.README
  /usr/share/doc/libvterm/COPYING
  /usr/share/doc/libvterm/ChangeLog

(libvterm0)
  /usr/bin/cygvterm-0.dll

(libvterm-devel)
  /usr/include/vterm.h
  /usr/lib/libvterm.a
  /usr/lib/libvterm.dll.a
  /usr/lib/libvterm.la

------------------

Port Notes:

----- version 0.99.7-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

