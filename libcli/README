libcli
------------------------------------------
Libcli provides a shared library for including a Cisco-like command-line
interface into other software. It's a telnet interface which supports
command-line editing, history, authentication and callbacks for a
user-definable function tree.

Runtime requirements:
  crypt-1.1-1
  cygwin-1.7.11-1

Build requirements:
(besides corresponding -devel packages)
  autoconf-10-1
  automake-4-10
  binutils-2.22.51-2
  cygport-0.10.9-1
  gawk-4.0.0-1
  gcc4-core-4.5.3-3
  libtool-2.4-1
  make-3.82.90-1

Canonical website:
  http://sites.dparrish.com/libcli

Canonical download:
  https://github.com/downloads/dparrish/libcli/libcli-1.9.5.tar.gz

-------------------------------------------

Build instructions:
  unpack libcli-1.9.5-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./libcli-1.9.5-X.cygport all

This will create:
  /usr/src/libcli-1.9.5-X-src.tar.bz2
  /usr/src/libcli-1.9.5-X.tar.bz2
  /usr/src/libcli1.9.5-1.9.5-X.tar.bz2
  /usr/src/libcli-devel-1.9.5-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(libcli)
  /usr/share/doc/Cygwin/libcli.README
  /usr/share/doc/libcli/COPYING
  /usr/share/doc/libcli/README

(libcli1.9.5)
  /usr/bin/cygcli-1-9-5.dll

(libcli-devel)
  /usr/include/libcli.h
  /usr/lib/libcli.dll.a
  /usr/lib/libcli.la

------------------

Port Notes:

----- version 1.9.5-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

