chromaprint
------------------------------------------
Chromaprint is the core component of the Acoustid project. It's a client-side
library that implements a custom algorithm for extracting fingerprints from
any audio source.

Runtime requirements:
  cygwin-1.7.13-1
  libavcodec53-0.10-2
  libavformat53-0.10-2
  libavutil51-0.10-2
  libfftw3_3-3.3-1
  libgcc1-4.5.3-3
  libstdc++6-4.5.3-3

Build requirements:
(besides corresponding -devel packages)
  binutils-2.22.51-2
  cmake-2.8.7-1
  cygport-0.10.10-1
  gcc4-core-4.5.3-3
  gcc4-g++-4.5.3-3
  make-3.82.90-1

Canonical website:
  http://acoustid.org/chromaprint

Canonical download:
  https://github.com/downloads/lalinsky/chromaprint/chromaprint-0.6.tar.gz

-------------------------------------------

Build instructions:
  unpack chromaprint-0.6-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./chromaprint-0.6-X.cygport all

This will create:
  /usr/src/chromaprint-0.6-X-src.tar.bz2
  /usr/src/chromaprint-0.6-X.tar.bz2
  /usr/src/libchromaprint0-0.6-X.tar.bz2
  /usr/src/libchromaprint-devel-0.6-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(chromaprint)
  /usr/bin/fpcalc.exe
  /usr/share/doc/Cygwin/chromaprint.README
  /usr/share/doc/chromaprint/CHANGES.txt
  /usr/share/doc/chromaprint/COPYING.txt
  /usr/share/doc/chromaprint/NEWS.txt
  /usr/share/doc/chromaprint/README.txt

(libchromaprint0)
  /usr/bin/cygchromaprint-0.dll

(libchromaprint-devel)
  /usr/include/chromaprint.h
  /usr/lib/libchromaprint.dll.a
  /usr/lib/pkgconfig/libchromaprint.pc

------------------

Port Notes:

----- version 0.6-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

