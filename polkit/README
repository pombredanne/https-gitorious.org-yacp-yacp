polkit
------------------------------------------
PolicyKit is an application-level toolkit for defining and handling the policy
that allows unprivileged processes to speak to privileged processes: It is a
framework for centralizing the decision making process with respect to
granting access to privileged operations for unprivileged applications.
PolicyKit is specifically targeting applications in rich desktop environments
on multi-user UNIX-like operating systems. It does not imply or rely on any
exotic kernel features.

Runtime requirements:
  cygwin-1.7.11-1
  libexpat1-2.0.1-1
  libgcc1-4.5.3-3
  libglib2.0_0-2.30.2-1

Build requirements:
(besides corresponding -devel packages)
  autoconf-10-1
  automake-4-10
  binutils-2.22.51-2
  cygport-0.10.9-1
  gawk-4.0.0-1
  gcc4-core-4.5.3-3
  libtool-2.4-1
  make-3.82.90-1

Canonical website:
  http://www.freedesktop.org/wiki/Software/PolicyKit

Canonical download:
  http://hal.freedesktop.org/releases/polkit-0.104.tar.gz

-------------------------------------------

Build instructions:
  unpack polkit-0.104-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./polkit-0.104-X.cygport all

This will create:
  /usr/src/polkit-0.104-X-src.tar.bz2
  /usr/src/polkit-0.104-X.tar.bz2
  /usr/src/libpolkit-agent1_0-0.104-X.tar.bz2
  /usr/src/libpolkit-agent1-devel-0.104-X.tar.bz2
  /usr/src/libpolkit-backend1_0-0.104-X.tar.bz2
  /usr/src/libpolkit-backend1-devel-0.104-X.tar.bz2
  /usr/src/libpolkit-gobject1_0-0.104-X.tar.bz2
  /usr/src/libpolkit-gobject1-devel-0.104-X.tar.bz2
  /usr/src/girepository-Polkit1.0-0.104-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(polkit)
  /etc/dbus-1/system.d/org.freedesktop.PolicyKit1.conf
  /usr/bin/pk-example-frobnicate.exe
  /usr/bin/pkaction.exe
  /usr/bin/pkcheck.exe
  /usr/bin/pkexec.exe
  /usr/lib/polkit-agent-helper-1.exe
  /usr/lib/polkitd.exe
  /usr/share/dbus-1/system-services/org.freedesktop.PolicyKit1.service
  /usr/share/doc/Cygwin/polkit.README
  /usr/share/doc/polkit/COPYING
  /usr/share/doc/polkit/HACKING
  /usr/share/doc/polkit/NEWS
  /usr/share/doc/polkit/README
  /usr/share/locale/da/LC_MESSAGES/polkit-1.mo
  /usr/share/man/man1/pkaction.1.gz
  /usr/share/man/man1/pkcheck.1.gz
  /usr/share/man/man1/pkexec.1.gz
  /usr/share/man/man8/pklocalauthority.8.gz
  /usr/share/man/man8/polkit.8.gz
  /usr/share/man/man8/polkitd.8.gz
  /usr/share/polkit-1/actions/org.freedesktop.policykit.examples.pkexec.policy
  /usr/share/polkit-1/actions/org.freedesktop.policykit.policy

(libpolkit-agent1_0)
  /usr/bin/cygpolkit-agent-1-0.dll

(libpolkit-agent1-devel)
  /usr/include/polkit-1/polkitagent/polkitagent.h
  /usr/include/polkit-1/polkitagent/polkitagentenumtypes.h
  /usr/include/polkit-1/polkitagent/polkitagentlistener.h
  /usr/include/polkit-1/polkitagent/polkitagentsession.h
  /usr/include/polkit-1/polkitagent/polkitagenttextlistener.h
  /usr/include/polkit-1/polkitagent/polkitagenttypes.h
  /usr/lib/libpolkit-agent-1.dll.a
  /usr/lib/libpolkit-agent-1.la
  /usr/lib/pkgconfig/polkit-agent-1.pc

(libpolkit-backend1_0)
  /etc/polkit-1/localauthority.conf.d/50-localauthority.conf
  /etc/polkit-1/nullbackend.conf.d/50-nullbackend.conf
  /usr/bin/cygpolkit-backend-1-0.dll
  /usr/lib/polkit-1/extensions/cygnullbackend.dll

(libpolkit-backend1-devel)
  /usr/include/polkit-1/polkitbackend/polkitbackend.h
  /usr/include/polkit-1/polkitbackend/polkitbackendactionlookup.h
  /usr/include/polkit-1/polkitbackend/polkitbackendauthority.h
  /usr/include/polkit-1/polkitbackend/polkitbackendinteractiveauthority.h
  /usr/include/polkit-1/polkitbackend/polkitbackendlocalauthority.h
  /usr/include/polkit-1/polkitbackend/polkitbackendtypes.h
  /usr/lib/libpolkit-backend-1.dll.a
  /usr/lib/libpolkit-backend-1.la
  /usr/lib/pkgconfig/polkit-backend-1.pc

(libpolkit-gobject1_0)
  /usr/bin/cygpolkit-gobject-1-0.dll

(libpolkit-gobject1-devel)
  /usr/include/polkit-1/polkit/polkit.h
  /usr/include/polkit-1/polkit/polkitactiondescription.h
  /usr/include/polkit-1/polkit/polkitauthority.h
  /usr/include/polkit-1/polkit/polkitauthorityfeatures.h
  /usr/include/polkit-1/polkit/polkitauthorizationresult.h
  /usr/include/polkit-1/polkit/polkitcheckauthorizationflags.h
  /usr/include/polkit-1/polkit/polkitdetails.h
  /usr/include/polkit-1/polkit/polkitenumtypes.h
  /usr/include/polkit-1/polkit/polkiterror.h
  /usr/include/polkit-1/polkit/polkitidentity.h
  /usr/include/polkit-1/polkit/polkitimplicitauthorization.h
  /usr/include/polkit-1/polkit/polkitpermission.h
  /usr/include/polkit-1/polkit/polkitprivate.h
  /usr/include/polkit-1/polkit/polkitsubject.h
  /usr/include/polkit-1/polkit/polkitsystembusname.h
  /usr/include/polkit-1/polkit/polkittemporaryauthorization.h
  /usr/include/polkit-1/polkit/polkittypes.h
  /usr/include/polkit-1/polkit/polkitunixgroup.h
  /usr/include/polkit-1/polkit/polkitunixnetgroup.h
  /usr/include/polkit-1/polkit/polkitunixprocess.h
  /usr/include/polkit-1/polkit/polkitunixsession.h
  /usr/include/polkit-1/polkit/polkitunixuser.h
  /usr/lib/libpolkit-gobject-1.dll.a
  /usr/lib/libpolkit-gobject-1.la
  /usr/lib/pkgconfig/polkit-gobject-1.pc

(girepository-Polkit1.0)
  /usr/lib/girepository-1.0/Polkit-1.0.typelib
  /usr/lib/girepository-1.0/PolkitAgent-1.0.typelib
  /usr/share/gir-1.0/Polkit-1.0.gir
  /usr/share/gir-1.0/PolkitAgent-1.0.gir

------------------

Port Notes:

----- version 0.104-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

