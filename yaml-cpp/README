yaml-cpp
------------------------------------------
yaml-cpp is a YAML parser and emitter in C++ matching the YAML 1.2 spec.

Runtime requirements:
  cygwin-1.7.10-1
  libgcc1-4.5.3-3
  libstdc++6-4.5.3-3

Build requirements:
(besides corresponding -devel packages)
  binutils-2.22.51-1
  cmake-2.8.4-1
  cygport-0.10.7-1
  gcc4-core-4.5.3-3
  gcc4-g++-4.5.3-3
  make-3.82.90-1

Canonical website:
  http://code.google.com/p/yaml-cpp/

Canonical download:
  http://yaml-cpp.googlecode.com/files/yaml-cpp-0.3.0.tar.gz

-------------------------------------------

Build instructions:
  unpack yaml-cpp-0.3.0-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./yaml-cpp-0.3.0-X.cygport all

This will create:
  /usr/src/yaml-cpp-0.3.0-X-src.tar.bz2
  /usr/src/yaml-cpp-0.3.0-X.tar.bz2
  /usr/src/libyaml-cpp0.3-0.3.0-X.tar.bz2
  /usr/src/libyaml-cpp-devel-0.3.0-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(yaml-cpp)
  /usr/share/doc/Cygwin/yaml-cpp.README
  /usr/share/doc/yaml-cpp/LICENSE.txt

(libyaml-cpp0.3)
  /usr/bin/cygyaml-cpp-0.3.dll

(libyaml-cpp-devel)
  /usr/include/yaml-cpp/aliasmanager.h
  /usr/include/yaml-cpp/anchor.h
  /usr/include/yaml-cpp/binary.h
  /usr/include/yaml-cpp/contrib/anchordict.h
  /usr/include/yaml-cpp/contrib/graphbuilder.h
  /usr/include/yaml-cpp/conversion.h
  /usr/include/yaml-cpp/dll.h
  /usr/include/yaml-cpp/emitfromevents.h
  /usr/include/yaml-cpp/emitter.h
  /usr/include/yaml-cpp/emittermanip.h
  /usr/include/yaml-cpp/eventhandler.h
  /usr/include/yaml-cpp/exceptions.h
  /usr/include/yaml-cpp/iterator.h
  /usr/include/yaml-cpp/ltnode.h
  /usr/include/yaml-cpp/mark.h
  /usr/include/yaml-cpp/node.h
  /usr/include/yaml-cpp/nodeimpl.h
  /usr/include/yaml-cpp/nodereadimpl.h
  /usr/include/yaml-cpp/nodeutil.h
  /usr/include/yaml-cpp/noncopyable.h
  /usr/include/yaml-cpp/null.h
  /usr/include/yaml-cpp/ostream.h
  /usr/include/yaml-cpp/parser.h
  /usr/include/yaml-cpp/stlemitter.h
  /usr/include/yaml-cpp/stlnode.h
  /usr/include/yaml-cpp/traits.h
  /usr/include/yaml-cpp/yaml.h
  /usr/lib/libyaml-cpp.dll.a
  /usr/lib/pkgconfig/yaml-cpp.pc

------------------

Port Notes:

----- version 0.3.0-1bl1 -----
Version bump.

----- version 0.2.6-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

