asdcplib
------------------------------------------
asdcplib is an open source implementation of SMPTE and the MXF Interop 'Sound
& Picture Track File' format. It was originally developed with support from
DCI. Development is currently supported by CineCert and other D-Cinema
manufacturers.

Runtime requirements:
  cygwin-1.7.11-1
  libgcc1-4.5.3-3
  libopenssl098-0.9.8t-1
  libstdc++6-4.5.3-3
  libxerces-c30-3.0.1-10

Build requirements:
(besides corresponding -devel packages)
  autoconf-10-1
  automake-4-10
  binutils-2.22.51-1
  cygport-0.10.8.1-1
  gawk-4.0.0-1
  gcc4-core-4.5.3-3
  gcc4-g++-4.5.3-3
  libtool-2.4-1
  make-3.82.90-1

Canonical website:
  http://www.cinecert.com/asdcplib/

Canonical download:
  http://www.cinecert.com/asdcplib/asdcplib-1.9.45.tar.gz

-------------------------------------------

Build instructions:
  unpack asdcplib-1.9.45-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./asdcplib-1.9.45-X.cygport all

This will create:
  /usr/src/asdcplib-1.9.45-X-src.tar.bz2
  /usr/src/asdcplib-1.9.45-X.tar.bz2
  /usr/src/libasdcp1.9_45-1.9.45-X.tar.bz2
  /usr/src/libasdcp-devel-1.9.45-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(asdcplib)
  /usr/bin/asdcp-info.exe
  /usr/bin/asdcp-test.exe
  /usr/bin/asdcp-unwrap.exe
  /usr/bin/asdcp-util.exe
  /usr/bin/asdcp-wrap.exe
  /usr/bin/blackwave.exe
  /usr/bin/j2c-test.exe
  /usr/bin/klvwalk.exe
  /usr/bin/kmfilegen.exe
  /usr/bin/kmrandgen.exe
  /usr/bin/kmuuidgen.exe
  /usr/bin/wavesplit.exe
  /usr/share/doc/Cygwin/asdcplib.README
  /usr/share/doc/asdcplib/COPYING
  /usr/share/doc/asdcplib/README

(libasdcp1.9_45)
  /usr/bin/cygasdcp-1-9-45.dll
  /usr/bin/cygkumu-1-9-45.dll

(libasdcp-devel)
  /usr/include/AS_DCP.h
  /usr/include/KM_error.h
  /usr/include/KM_fileio.h
  /usr/include/KM_log.h
  /usr/include/KM_memio.h
  /usr/include/KM_mutex.h
  /usr/include/KM_platform.h
  /usr/include/KM_prng.h
  /usr/include/KM_tai.h
  /usr/include/KM_util.h
  /usr/include/KM_xml.h
  /usr/lib/libasdcp.dll.a
  /usr/lib/libasdcp.la
  /usr/lib/libkumu.dll.a
  /usr/lib/libkumu.la

------------------

Port Notes:

----- version 1.9.45-1bl1 -----
Version bump.

----- version 1.8.41-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

