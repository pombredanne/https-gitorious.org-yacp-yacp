confuse
------------------------------------------
libConfuse is a configuration file parser library, licensed under the terms of
the ISC license, and written in C. It supports sections and (lists of) values
(strings, integers, floats, booleans or other sections), as well as some other
features (such as single/double-quoted strings, environment variable expansion,
functions and nested include statements). It makes it very easy to add
configuration file capability to a program using a simple API.

Runtime requirements:
  cygwin-1.7.7-1
  libgcc1-4.3.4-3
  libintl8-0.17-11

Build requirements:
(besides corresponding -devel packages)
  autoconf-9-1
  automake-4-10
  binutils-2.20.51-2
  cygport-0.10.2-1
  gawk-3.1.8-1
  gcc4-core-4.3.4-3
  libtool-2.4-1
  make-3.81-2

Canonical website:
  http://www.nongnu.org/confuse/

Canonical download:
  http://savannah.nongnu.org/download/confuse/confuse-2.7.tar.gz

-------------------------------------------

Build instructions:
  unpack confuse-2.7-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./confuse-2.7-X.cygport all

This will create:
  /usr/src/confuse-2.7-X-src.tar.bz2
  /usr/src/confuse-2.7-X.tar.bz2
  /usr/src/libconfuse0-2.7-X.tar.bz2
  /usr/src/libconfuse-devel-2.7-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(confuse)
  /usr/share/doc/Cygwin/confuse.README
  /usr/share/doc/confuse/AUTHORS
  /usr/share/doc/confuse/NEWS
  /usr/share/doc/confuse/README

(libconfuse0)
  /usr/bin/cygconfuse-0.dll
  /usr/share/locale/fr/LC_MESSAGES/confuse.mo
  /usr/share/locale/sv/LC_MESSAGES/confuse.mo

(libconfuse-devel)
  /usr/include/confuse.h
  /usr/lib/libconfuse.a
  /usr/lib/libconfuse.dll.a
  /usr/lib/libconfuse.la
  /usr/lib/pkgconfig/libconfuse.pc
  /usr/share/man/man3/cfg_defvalue_t.3.gz
  /usr/share/man/man3/cfg_opt_t.3.gz
  /usr/share/man/man3/cfg_t.3.gz
  /usr/share/man/man3/cfg_value_t.3.gz
  /usr/share/man/man3/confuse.h.3.gz

------------------

Port Notes:

----- version 2.7-1bl2 -----
Repackage for licensing & readmes

----- version 2.7-1bl1 -----
Version bump.

----- version 2.6-1bl2 -----
Rebuild with gcc-4 by fd0 <http://d.hatena.ne.jp/fd0/>

----- version 2.6-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

