pdfcube
------------------------------------------
PDF Cube uses Poppler and OpenGL APIs to add 3D spinning cube page transitions
to PDF documents.

Runtime requirements:
  cygwin-1.7.11-1
  libboost1.48-1.48.0-1
  libcairo2-1.10.2-1
  libgcc1-4.5.3-3
  libGL1-7.11.2-1
  libglib2.0_0-2.30.2-1
  libGLU1-7.11.2-1
  libgtk2.0_0-2.24.10-1
  libgtkglext1.0_0-1.2.0-10
  libpoppler-glib8-0.18.4-2
  libstdc++6-4.5.3-3

Build requirements:
(besides corresponding -devel packages)
  autoconf-10-1
  automake-4-10
  binutils-2.22.51-1
  cygport-0.10.9-1
  gawk-4.0.0-1
  gcc4-core-4.5.3-3
  gcc4-g++-4.5.3-3
  libtool-2.4-1
  make-3.82.90-1

Canonical website:
  http://code.100allora.it/pdfcube

Canonical download:
  http://code.100allora.it/releases/pdfcube/pdfcube-0.0.4-beta.tar.gz

-------------------------------------------

Build instructions:
  unpack pdfcube-0.0.4-beta-X-src.tar.bz2
    if you use setup to install this src package, it will be
	 unpacked under /usr/src automatically
  cd /usr/src
  cygport ./pdfcube-0.0.4-beta-X.cygport all

This will create:
  /usr/src/pdfcube-0.0.4-beta-X-src.tar.bz2
  /usr/src/pdfcube-0.0.4-beta-X.tar.bz2

-------------------------------------------

Files included in the binary package:

(pdfcube)
  /usr/bin/pdfcube.exe
  /usr/share/doc/Cygwin/pdfcube.README
  /usr/share/doc/pdfcube/AUTHORS
  /usr/share/doc/pdfcube/COPYING
  /usr/share/doc/pdfcube/ChangeLog
  /usr/share/doc/pdfcube/README

------------------

Port Notes:

----- version 0.0.4-beta-1bl2 -----
Rebuild for libboost

----- version 0.0.4-beta-1bl1 -----
Version bump.

----- version 0.0.3-1bl3 -----
Rebuild for libpoppler-glib

----- version 0.0.3-1bl2 -----
Rebuild for libpoppler-glib

----- version 0.0.3-1bl1 -----
Initial release for Cygwin-1.7 by fd0 <http://d.hatena.ne.jp/fd0>

